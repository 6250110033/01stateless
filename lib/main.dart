import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.insert_emoticon_rounded),
          title: Text('My PangPang'),
          actions: [IconButton(onPressed: (){}, icon: Icon(Icons.add_to_drive))],
        ),
        body: Center(
          child:Text('Irfan  Hama.',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 35
            ),),
        ),
      ),
    );
  }
}